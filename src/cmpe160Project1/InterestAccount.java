package cmpe160Project1;


/** Represents Account with interest rate.
 * 
 * @author yigitozgumus
 * */
public class InterestAccount extends BankAccount {
	
	private static double interestRate = 1;
	protected Bank theBank = null ;
	
	
	/** Constructor for the interestAccount class. Uses balance parameter to update the
	 * current balance.
	 * 
	 * @param balance The amount that will be used to update the balance of the account.
	 * @param theBank the bank object that account is in
	 * */
	public InterestAccount(double balance,Bank theBank) {
		super(balance,theBank);
	}
	/**A setter method to change the global fixed interest rate. Doesn't return anything.
	 * Has one double type parameter.
	 * 
	 * @param rate the new rate for the accounts with interest
	 * */
	public static void setInterestRate(double rate){
		interestRate = rate ;
	}
	/** Method calculates the estimated balance in given time. It has one integer parameter
	 * to determine the number of days. uses a for loop to calculate the total amount with the
	 * interest and returns a double type as the final result.
	 * 
	 * @param totalDays number of days that will pass during the estimation
	 * @return Balance with interest and it is rounded for two decimal precision.
	 * */
	public double calculateEstimatedBalance(int totalDays){
		double totalBalance = 0 ;
		double amount = this.getBalance();
		for(int day = 0 ; day < totalDays ; day++){
			totalBalance = amount + (amount * (interestRate/100));
			amount = totalBalance ;
		}
			 return Math.round(totalBalance * 100.0)/ 100.0;
		}
	
	/** Returns the basic information about the interest account. Method gets the id and the current 
	 * balance of the target object via getter methods and adds them to a string. Also it displays the 
	 * balance with a interest rate that is specified by the user. The time limit is 365 days.
	 * Method returns the transactions via the listTransactions method.
	 * Returns a string containing all that information.
	 * 
	 * @return The concatenated information about the interest account object
	 * */
	@Override
	public String toString() {
		
		String estimatedBalance = "Estimated balance in 365 days is: " +
		formatter.format(calculateEstimatedBalance(365));
		String transactionList = listTransactions(10) ;

		String finalInfo = ("id: " + this.getId() +
				"\nBalance: " +formatter.format(this.getBalance())
				+ "\n" + estimatedBalance +"\nTransactions: \n"
				+ transactionList); 
		return finalInfo  ;
		
	}
}
