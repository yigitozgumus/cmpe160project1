package cmpe160Project1;

import java.text.DecimalFormat;

/**Represents the transaction process.
 * 
 * @author yigitozgumus
 * */
public class Transaction {
	
	protected int sendingAccountId ;
	protected int receivingAccountId ;
	protected double transactionAmount;
	DecimalFormat formatter = new DecimalFormat("#0.00");
	
	/** Constructor for the Transaction object. Stores the receiver and sender id,
	 * also the amount of the current. Consequently it uses three parameters.
	 * 
	 * @param sender the id number of the sender account
	 * @param receiver the id number of the receiver account
	 * @param amount amount of money that has been transferred
	 * */
	public Transaction(int sender,int receiver,double amount){
		this.sendingAccountId = sender ;
		this.receivingAccountId = receiver ;
		this.transactionAmount = amount ;
	}
	
	/**Sends a general information about the current transaction.
	 * Returns a string. Contains sender, receiver id and the amount of transaction.
	 * 
	 * @return Info about sender receiver and the amount info of the transaction
	 * */
	@Override
	public String toString(){
		String info = "From: " + this.sendingAccountId 
				+ ", To: " + this.receivingAccountId
				+", Amount: " + formatter.format(this.transactionAmount); 
		return (info); 
				
	}

}
