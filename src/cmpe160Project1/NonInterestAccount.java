package cmpe160Project1;



/**Represents Account class without interest rate.
 * 
 * @author yigitozgumus
 * */
public class NonInterestAccount extends BankAccount {
	 

	/** Constructor for the noninterestAccount. It uses Constructor of the 
	 * Bank class for id production. takes one double type parameter and one
	 * Bank parameter.
	 * 
	 * @param balance balance of the current account.
	 * @param theBank the bank object that account belongs to
	 *  */
	public NonInterestAccount(double balance,Bank theBank) {
		super(balance,theBank);
	}

	/** Returns the information about the account. It gets the id and the balance
	 * with getter methods and it returns the related transactions with listTransactions method.
	 * 
	 * @return Bundled info about the transactions
	 * */
	@Override
	public String toString() {
		String transactionList = "id: " + this.getId() +
				"\nBalance: " +formatter.format(this.getBalance()) + "\nTransactions: \n" ;
		transactionList += listTransactions(10);
		return (transactionList);
	}
	/**Does not return anything since it's a non interest account. 
	 * But it must be implemented with a empty body since method is abstract declaration.
	 * 
	 * @return dummy value for the continuation
	 * */
	@Override
	public double calculateEstimatedBalance(int n) {
		System.out.println("This account is non-interest type.");
		return 0;
	}

	

}
