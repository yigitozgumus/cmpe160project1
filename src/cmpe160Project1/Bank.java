 package cmpe160Project1;
import java.text.DecimalFormat;
import java.util.ArrayList;

/**Implements Bank and control its accounts. 
 * 
 * @author yigitozgumus
 * */
public class Bank {
	
	private  ArrayList <BankAccount> Accounts = new ArrayList<BankAccount>();
	private  ArrayList <Transaction> Transactions = new ArrayList<Transaction>();
	DecimalFormat formatter = new DecimalFormat("#0.00");
	
	/**Constructor for the bank instance.
	 * 
	 * */
	public Bank() {
		
	}
	/** Returns the correct account type. it Uses a for loop to search through the arraylist and
	 *  checks the type with the instanceof method. Returns a string.
	 *  
	 *  @param idNumber id number of the target account
	 *  @return String representation of the type of the account
	 * */
	public String getAccountType(int idNumber){
		String type = "";
		for(int index = 0 ; index <= getAccounts().size()-1; index++){
			if(idNumber == getAccounts().get(index).getId()){
				if(getAccounts().get(index) instanceof InterestAccount) {
					type = "Interest";
				}else{
					type = "Non-Interest";
				}
			}
		}
		return type ;
	}
	/** Return the correct account object with a given id. Uses a for loop to iterate through
	 * the account array list and returns the owner of given id. If the id is incorrect, it 
	 * throws an exception.
	 * 
	 * @param id id of the target account
	 * @return the wanted account
	 * @exception NullPointerException for the wrong id just in case
	 * */
	public BankAccount getAccount(int id) throws NullPointerException{
		try{
			BankAccount currentAccount = null ;
			for(int index = 0 ; index < getAccounts().size();index++){
				if(id == getAccounts().get(index).getId()){
					currentAccount = getAccounts().get(index);
				}
			}
			if(currentAccount == null){
				throw new NullPointerException();
			}
			return currentAccount ;
		}catch(NullPointerException e){
			System.out.println("There is no account with that id.");
			return null;	
		}
	}
	/**Returns the Accounts Array list in the Bank class.
	 * 
	 * @return List of accounts of the current bank object
	 * */
	public  ArrayList <BankAccount> getAccounts() {
		return Accounts;
	}
	/**Standard setter method for the Accounts Array list.
	 * 
	 * @param accounts Array list of the Bank Class
	 * */
	public void setAccounts(ArrayList <BankAccount> accounts) {
		Accounts = accounts;
	}
	/** Returns the transactions array list in the Bank Class
	 * 
	 * @return List for transactions of the current bank object.
	 * */
	public ArrayList <Transaction> getTransactions() {
		return Transactions;
	}
	/**Standard setter method for transactions array list.
	 * */
	public void setTransactions(ArrayList <Transaction> transactions) {
		Transactions = transactions;
	}

	/**Opens an account with the type of non-interest. This method
	 * returns nothing. It uses getAccounts() method to add an account
	 * to the Accounts array list and uses Accounts class methods to notify the user
	 * About successful creation.
	 * 
	 * @param balance it is the beginning balance of the account
	 * */
	public void openNonInterestAccount(double balance){
		if(balance >= 0 ){
			getAccounts().add(new NonInterestAccount(balance,this));
			
			
			System.out.println("Your account is created. ID : " + getAccounts().get(getAccounts().size()-1).getId() +
				" Balance: " + formatter.format(getAccounts().get(getAccounts().size()-1).getBalance()));	
		}else {
			System.out.println("You can't create an account with negative balance ");
		}
	}
	/**Opens a non interest account with a fixed zero balance. Uses the same method
	 * with the parameters.
	 * */
	public void openNonInterestAccount(){
		openNonInterestAccount(0);
	}
	/**Opens an account with the type of interest. Returns nothing.
	 * Again, it uses getAccounts() method to add an account to the 
	 * Accounts array list.
	 * 
	 * @param balance balance of the current account
	 * */
	public void openInterestAccount(double balance){
			if(balance >= 0){
				getAccounts().add(new InterestAccount(balance,this));
				
				System.out.println("Your account is created. ID : " + getAccounts().get(getAccounts().size()-1).getId() +
					" Balance: " + formatter.format(getAccounts().get(getAccounts().size()-1).getBalance()));
			}else {
				System.out.println("You can't create an account with negative balance ");
			}
	}
	/**Opens an interest account with a fixed zero balance. Uses the same method
	 * with the parameters.
	 * */
	public void openInterestAccount(){
		openInterestAccount(0);
	}
	/**Closes the chosen account. It uses getAccounts() method to access to the 
	 * Array list then it uses its remove method to delete the account.
	 * 
	 * @param index the current index of the wanted account in the array list
	 * */
	public void closeAccount(int id){
		getAccounts().remove(getAccount(id));
	}
	/** Returns the string representation of the Bank. Returns the String representation
	 * of all of the accounts that stored in the Accounts array list.
	 * It uses a for loop to access the elements of the Accounts array list
	 * then uses its fields to create the proper string information.
	 * 
	 * @return Basic information about the accounts of the current bank
	 * */
	@Override
	public String toString(){
		String information = "";
		for(int i = 0 ; i < getAccounts().size() ; i++){
			int id = getAccounts().get(i).getId() ;
			double balance = getAccounts().get(i).getBalance();
			information += "Account ID: "+ id + ", Balance: " +
			formatter.format(balance) +", Type: " + getAccountType(id) + "\n" ;
		}
			return information ;
	}
	
	
}

