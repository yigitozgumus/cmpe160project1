package cmpe160Project1;

import java.text.DecimalFormat;
import java.util.Random;
/** Creates Bank accounts and contain related methods.
 * 
 * @author yigitozgumus
 * */

public abstract class BankAccount   {

	private Random assignId = new Random();
	DecimalFormat formatter = new DecimalFormat("#0.00");

	protected double balance ;

	private int id  ; 
	protected int receiverId ;
	protected int senderId ;

	protected Bank currentBank ;


	/** Returns receiver id for the transaction.
	 *  
	 *  @return Receiver part of the transaction
	 * */
	public int getReceiverId() {
		return receiverId;
	}
	/** Sets the receiver id for the transaction.Returns void.
	 * 
	 * @param receiverId it is the new value that will be replaced 
	 * */
	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
	/** Returns the sender id for the transaction.
	 * 
	 * @return Sender part of the transaction 
	 * */
	public int getSenderId() {
		return senderId;
	}
	/** Sets the sender if for the transaction. Returns void.
	 * 
	 *  @param senderId variable information for the sender id.
	 * */
	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}
	/** Returns the current balance of the account. Type is double.
	 * 
	 * @return Balance of the current account
	 * */
	public double getBalance(){
		return this.balance ;
	}
	/** Replaces the balance with the new amount. 
	 * 
	 * @param newAmount amount to be replaced.
	 * */
	public void setBalance(double newAmount){
		this.balance = newAmount ;
	}
	/** Default Constructor of the BankAccount class.
	 *  Takes a bank parameter. Current Balance is predetermined zero.
	 *  
	 *  @param theBank the current bank object that account belongs to
	 */
	protected BankAccount(Bank theBank){
		this(0,theBank);
		this.currentBank = theBank;


	}
	/**constructor for the bankAccount. Constructor Assigns an
	 * unique id number to the opened account using a final variable
	 * and a random number account generator. 
	 * 
	 * @param balance wanted balance for the accounts
	 * @param theBank the current bank object that account belongs to
	 * 
	 **/
	protected BankAccount(double balance,Bank theBank){
		boolean uniqueness = true ;
		if(theBank.getAccounts().size() == 0){
			this.id = 100 + assignId.nextInt(900);
		}else{
			this.id = 100 + assignId.nextInt(900);
			do{
				for(int index = 0 ; index < theBank.getAccounts().size();index++){
					if(id == theBank.getAccounts().get(index).getId()){
						uniqueness = false ;
						this.id = 100 + assignId.nextInt(900);
					}else{
						uniqueness = true ;
					}
				}
			}while(!uniqueness);
		}
		this.balance = balance ;
		this.currentBank = theBank;

	}
	/**Getter method for the id of the wanted account object.
	 * Returns the Id field of the object.
	 * 
	 * @return Unique id of any account that is created.
	 *  */
	public int getId() {
		return id;
	}
	/** Declaration of the toString method for the subclasses.
	 * 
	 * */
	@Override 
	public abstract String toString();
	/** Abstract method for the calculation of the balance with interest
	 * 
	 * @param days number of days with the interest
	 * */
	public abstract double calculateEstimatedBalance(int days);

	/**	Transfers given amount of money to target account. Method has two parameters 
	 * one integer and one double. takes the id of the caller and uses for loop to find the
	 * id of the receiver account than uses deposit and withdraw methods on related accounts and
	 * creates a transaction object to save the transfer.
	 * It has a failure check in case for insufficient balance amounts.
	 * 
	 * @param receiver id number of the target account
	 * @param amount the amount of money to be transferred
	 * 
	 * */
	public void transaction(int receiver,double amount){

		if(amount > 0) {
			for(int index = 0 ; index < currentBank.getAccounts().size();index++){
				if(receiver == currentBank.getAccounts().get(index).getId()){
					setReceiverId(index) ;
				}
				else if(this.getId() == currentBank.getAccounts().get(index).getId()){
					setSenderId(index) ;
				}
			}

			if(this.getBalance() < amount){
				System.out.println("Transaction Failure.");
			}else {
				currentBank.getTransactions().add(new Transaction(this.getId(),receiver,amount));
				currentBank.getAccounts().get(getSenderId()).withdraw(amount);
				currentBank.getAccounts().get(getReceiverId()).deposit(amount);
				System.out.println("Transaction Success.");

			}
		}else {
			System.out.println("You can't do transaction with negative amount. ");
		}
	}

	/** Makes the deposit to the target account. Uses setter and getter methods of the
	 * balance variable. Does not return anything. displays a success message for user to check
	 * correct id.
	 * 
	 *  @param amount amount of money that will be added
	 * */
	public void deposit(double amount) {
		if(amount > 0){
			this.setBalance(this.getBalance() + amount);
			System.out.println("Deposit is Successful for ID: " + this.getId() +
					" New balance: " + formatter.format(this.getBalance()));
		}else if(amount == 0){
			System.out.println("Balance is the same for ID: " + this.getId() +
					" Balance: " + formatter.format(this.getBalance()));
		}else{
			System.out.println("Deposit failure, you can't deposit negative amounts.");
		}

	}

	/**Makes the withdraw process from the target account. Uses setter and getter methods of the
	 * balance variable. Doesn't return anything. It has a simple failure check in case of insufficient balance
	 * with a simple if statement. For success it displays a String containing the information about the process
	 * 
	 * @param amount amount of money that will be subtracted
	 * */
	public void withdraw(double amount) {
		if(amount > 0){
			if(this.getBalance() - amount > 0){
				this.setBalance(this.getBalance() - amount) ;
				System.out.println("Withdraw Success for ID: " + this.getId() +
						" New balance: " + formatter.format(this.getBalance()));
			}else if(amount == 0){
				System.out.println("Balance is the same for ID: " + this.getId() +
						" Balance: " + formatter.format(this.getBalance()));
			} else{
				System.out.println("Withdraw Failure");
			}
		}else {
			System.out.println("Withdraw failure, you can't withdraw negative amounts.");
		}
	}
	/** List the transactions for the target account with specified amount. Method uses one integer parameter
	 * to limit the printing(description regulation). It checks the size of the transaction array and chooses 
	 * how many transaction will it print with a number of nested if statements in the for loop. 
	 * Then checks whether the target object's id is receiver or sender. If it is, it prints the transaction
	 * with the toString method of the transaction class.
	 * 
	 * @param number number of transactions to be listed.
	 * @return The transaction list with the specified number
	 * */
	public String listTransactions(int number){
		int transactionCount = number ;
		if(number > currentBank.getTransactions().size()){
			transactionCount =  currentBank.getTransactions().size();
		}
		int transactionIndex = currentBank.getTransactions().size()-1 ;
		int size = currentBank.getTransactions().size()-1;
		int transactionTurn = 1 ;
		String finalList = "";
		for(int index = size ; index >= 0 ;index--){
			while(transactionCount > 0 && transactionIndex >= 0){
				if(currentBank.getTransactions().size() != 0){
					if(this.getId() == currentBank.getTransactions().get(transactionIndex).receivingAccountId
							|| this.getId() == currentBank.getTransactions().get(transactionIndex).sendingAccountId) {
						finalList +=(transactionTurn +". ")+
						currentBank.getTransactions().get(transactionIndex).toString() + "\n";
						transactionIndex -- ;
						transactionCount-- ;
						transactionTurn++;
					}else {
						transactionIndex -- ;
					}
				}
			}
		}
		return finalList;
	}
}
